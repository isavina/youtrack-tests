package issueCreateSmoke;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import components.IssuePriorityEnum;
import components.IssueStateEnum;
import components.IssueTypeEnum;
import components.loaders.PropertyLoader;
import components.models.Issue;
import components.models.User;
import components.robots.IssueCreationRobot;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import lombok.extern.slf4j.Slf4j;
import org.testng.TestListenerAdapter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import com.codeborne.selenide.testng.ScreenShooter;

import java.util.Calendar;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.closeWindow;

@Listeners({ScreenShooter.class, TestListenerAdapter.class})
@Slf4j
public class BaseTest {

    IssueCreationRobot issueCreationRobot = new IssueCreationRobot();
    protected Issue issue;

    @BeforeSuite
    @Step("Set up")
    public void setUpConfiguration() {
        Configuration.timeout = 20000;
        Configuration.headless = true;
        Configuration.baseUrl = PropertyLoader.loadProperty("cloudUrl");
        Configuration.browser = PropertyLoader.loadProperty("browser");
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
//        Configuration.startMaximized = true;
    }

    @AfterMethod
    @Step("Log out")
    public void tearDown() {
        SelenideLogger.removeListener("AllureSelenide");
        closeWindow();
        closeWebDriver();
    }

    public Issue createIssueModel(IssueTypeEnum issueType, IssueStateEnum issueState, IssuePriorityEnum issuePriority) {
        String title = "Test issue " + Calendar.getInstance().getTime();
        String description = "Description of test issue " + Calendar.getInstance().getTime();
        return new Issue(title, description, issueType, issueState, issuePriority);

    }

    protected void loginAsAdmin() {
        User adminUser = new User(PropertyLoader.loadProperty("adminEmail"), PropertyLoader.loadProperty("adminPassword"));
        issueCreationRobot.loginAsUser(adminUser);
    }

    protected void loginAsViewer() {
        User viewerUser = new User(PropertyLoader.loadProperty("viewerEmail"), PropertyLoader.loadProperty("viewerPassword"));
        issueCreationRobot.loginAsUser(viewerUser);
    }
}
