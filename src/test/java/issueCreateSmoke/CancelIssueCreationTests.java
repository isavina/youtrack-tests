package issueCreateSmoke;

import components.IssuePriorityEnum;
import components.IssueStateEnum;
import components.IssueTypeEnum;
import io.qameta.allure.*;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

@Slf4j
@Epic("YouTrack Smoke")
@Feature("Functional")
@Story("Create issue smoke tests")
public class CancelIssueCreationTests extends BaseTest {

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Close issue dialog of empty draft")
    public void cancelIssueCreation() {
        loginAsAdmin();
        issueCreationRobot.openIssueDialog();
        issueCreationRobot.cancelEmptyIssueDraft();
        issueCreationRobot.checkIssueIsNotCreated();
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Close issue dialog with saving a draft")
    public void cancelIssueCreationSaveDraft() {
        issue = createIssueModel(IssueTypeEnum.TASK, IssueStateEnum.OPEN, IssuePriorityEnum.MINOR);
        loginAsAdmin();
        issueCreationRobot.openIssueDialog();
        issueCreationRobot.addIssueDetails(issue);
        issueCreationRobot.cancelCreationSaveDraft();
        issueCreationRobot.checkIssueIsNotCreated();
    }
}
