package issueCreateSmoke;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

public class IssueCreationPermissionCheckTests extends BaseTest {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Check that read only user can't create an issue is issues tab")
    public void checkROUserCantCreateIssue() {
        loginAsViewer();
        issueCreationRobot.checkCreateBtnNotPresented();
    }
}
