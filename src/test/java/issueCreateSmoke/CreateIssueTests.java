package issueCreateSmoke;

import components.IssuePriorityEnum;
import components.IssueStateEnum;
import components.IssueTypeEnum;
import io.qameta.allure.*;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

@Slf4j
@Epic("YouTrack Smoke")
@Feature("Functional")
@Story("Create issue smoke tests")
public class CreateIssueTests extends BaseTest {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Create issue via Issues tab in dialog view")
    public void createIssueInDialogAsAdminIsOk() {
        issue = createIssueModel(IssueTypeEnum.TASK, IssueStateEnum.OPEN, IssuePriorityEnum.MINOR);
        loginAsAdmin();
        issueCreationRobot.openIssueDialog();
        issueCreationRobot.addIssueDetails(issue);
        issueCreationRobot.submitIssue();
        issueCreationRobot.checkIssueInIssuesList();
        issueCreationRobot.checkIssueParameters(issue);
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Create issue via Issues tab in full window view")
    public void createIssueFullWindowAsAdminIsOk() {
        issue = createIssueModel(IssueTypeEnum.FEATURE, IssueStateEnum.TO_BE_DISCUSSED, IssuePriorityEnum.MINOR);
        loginAsAdmin();
        issueCreationRobot.openIssueDialog();
        issueCreationRobot.createIssueInFullPage(issue);
        issueCreationRobot.goBackToIssues();
        issueCreationRobot.checkIssueInIssuesList();
        issueCreationRobot.checkIssueParameters(issue);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Create issue via Issues tab in dialog view using hotkeys")
    public void createIssueHotKeysAsAdminIsOk() {
        issue = createIssueModel(IssueTypeEnum.TASK, IssueStateEnum.OPEN, IssuePriorityEnum.MINOR);
        loginAsAdmin();
        issueCreationRobot.openIssueDialogHotKeys();
        issueCreationRobot.addIssueDetails(issue);
        issueCreationRobot.submitIssue();
        issueCreationRobot.checkIssueInIssuesList();
        issueCreationRobot.checkIssueParameters(issue);
    }
}
