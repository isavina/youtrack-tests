package components.robots;

import components.models.Issue;
import components.models.User;
import components.pages.*;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class IssueCreationRobot {

    LoginPage loginPage;
    IssuesPage issuesPage;
    BasePage startPage;
    IssueCreationDialog issueCreationDialog;
    IssueCreationPage issueCreationPage;
    String issueId;
    CreatedIssueDetailsPage createdIssueDetailsPage;
    IssueDetailsDialog issueDetailsDialog;
    Integer issuesBeforeTest;
    Integer issuesAfterTest;

    @Step("Log In to YouTrack")
    public void loginAsUser(User user) {
        loginPage = open(baseUrl, LoginPage.class);
        log.info("Log in page is opened");
        startPage = loginPage.setUsername(user.getEMail())
                .setPassword(user.getPassword())
                .clickLoginBtn();
        log.info("User is logged in");
        issuesPage = startPage.openIssuesTab();
        log.info("Issues page is opened");
        issuesBeforeTest = catchIssues();
    }

    @Step("Start issue creation in Issue dialog")
    public void openIssueDialog() {
        log.info("Start creating an issue");
        issueCreationDialog = issuesPage.openIssuesTab()
                .openCreateIssueDialog();
        log.info("Issue dialog is opened");
    }

    @Step("Start issue creation in Issue dialog using hotkeys")
    public void openIssueDialogHotKeys() {
        log.info("Start creating an issue");
        issueCreationDialog = issuesPage.openIssuesTab()
                .openCreateIssueDialogHotKeys();
    }

    @Step("Fill issue details")
    public void addIssueDetails(Issue issue) {
        issueCreationDialog = issueCreationDialog.fillSummary(issue.getSummary())
                .fillDescription(issue.getDescription())
                .selectIssueType(issue.getType())
                .selectIssueState(issue.getState())
                .selectIssuePriority(issue.getPriority());
    }

    @Step("Submit issue")
    public void submitIssue() {
        issuesPage = issueCreationDialog.submitIssue();
        issueId = issuesPage.checkAlertAppearsAndDisappears();
        log.info("Issue is created");
    }

    @Step("Check issue in issues list")
    public void checkIssueInIssuesList() {
        issuesAfterTest = catchIssues();
        assertThat(issuesAfterTest)
                .describedAs("Amount of issues after creation")
                .isGreaterThan(issuesBeforeTest)
                .describedAs("Amount of issues before test");
        log.info("Amount of issues is increased");
        log.info("Issue is successfully checked in the list");
    }

    @Step("Check issue parameters")
    public void checkIssueParameters(Issue issue) {
        issueDetailsDialog = issuesPage.openIssueDetails(issue.getSummary())
                .expandIssueFields();
        String actualIssueType = getActualIssueType();
        String actualIssueState = getActualIssueState();
        String actualIssuePriority = getActualIssuePriority();
        SoftAssertions softAssertion = new SoftAssertions();
        softAssertion.assertThat(actualIssueType)
                .describedAs("Issue type")
                .isEqualTo(issue.getType().getTitleValue())
                .describedAs("Expected issue type");
        softAssertion.assertThat(actualIssueState)
                .describedAs("Issue state")
                .isEqualTo(issue.getState().getTitleValue())
                .describedAs("Expected issue state");
        softAssertion.assertThat(actualIssuePriority)
                .describedAs("Issue priority")
                .isEqualTo(issue.getPriority().getTitleValue())
                .describedAs("Expected issue priority");
        softAssertion.assertThat(issueDetailsDialog.getIssueId())
                .describedAs("Issue id in details")
                .isEqualTo(issueId)
                .describedAs("Issue id from alert");
        softAssertion.assertAll();
        log.info("Issues parameters are checked");
    }

    @Step("Cancel empty issue draft")
    public void cancelEmptyIssueDraft() {
        issuesPage = issueCreationDialog.cancelIssueCreationDialog();
        issuesPage.checkAlertNotAppeared();
        log.info("Issue creation cancelled");
    }

    @Step("Cancel issue creation and save draft")
    public void cancelCreationSaveDraft() {
        issuesPage = issueCreationDialog.cancelCreationSaveDraft();
        log.info("Issue creation cancelled, draft saved");
        issuesPage.checkAlertNotAppeared();
        log.info("Issue creation alert is not appeared");
    }

    @Step("Check issue in not created")
    public void checkIssueIsNotCreated() {
        issuesAfterTest = catchIssues();
        assertThat(issuesAfterTest)
                .describedAs("Amount of issues after creation")
                .isEqualTo(issuesBeforeTest)
                .describedAs("Amount of issues before test");
        log.info("Amount of issues stays the same");
        log.info("Issue is not created");
    }

    @Step("Check that New Issue button is not presented for read-only user")
    public void checkCreateBtnNotPresented() {
        issuesPage.createBtnNotFound();
    }

    @Step("Proceed issue creation in full page")
    public void createIssueInFullPage(Issue issue) {
        log.info("Proceed in full view");
        issueCreationPage = issueCreationDialog.viewIssueInFullPage()
                .fillSummary(issue.getSummary())
                .fillDescription(issue.getDescription())
                .selectIssueType(issue.getType())
                .selectIssueState(issue.getState())
                .selectIssuePriority(issue.getPriority());
        createdIssueDetailsPage = issueCreationPage.submitIssue();
//        String pngFileName = screenshot("created_issue_details");
        log.info("Issue is created");
        String actualIssueType = createdIssueDetailsPage.getIssueType();
        String actualIssueState = createdIssueDetailsPage.getIssueState();
        String actualIssuePriority = createdIssueDetailsPage.getIssuePriority();
        issueId = createdIssueDetailsPage.getIssueId();
        SoftAssertions softAssertion = new SoftAssertions();
        softAssertion.assertThat(actualIssueType)
                .describedAs("Issue type")
                .isEqualTo(issue.getType().getTitleValue())
                .describedAs("Expected issue type");
        softAssertion.assertThat(actualIssueState)
                .describedAs("Issue state")
                .isEqualTo(issue.getState().getTitleValue())
                .describedAs("Expected issue state");
        softAssertion.assertThat(actualIssuePriority)
                .describedAs("Issue priority")
                .isEqualTo(issue.getPriority().getTitleValue())
                .describedAs("Expected issue priority");
        softAssertion.assertThat(issueId)
                .describedAs("Issue id in details")
                .isNotNull();
        softAssertion.assertAll();
        log.info("Issues parameters are checked on full page");
    }

    @Step("Go back to issues")
    public void goBackToIssues() {
        issuesPage = createdIssueDetailsPage.pressBackToIssuesBtn()
                .refreshPage();
//      Need to refresh page because otherwise new issue is not presented yet. It's a feature I hope :)
        log.info("Issues list is opened");
    }

    private Integer catchIssues() {
        String issuesBeforeTestString = issuesPage.getIssuesCounterValue();
        try {
            Integer.parseInt(issuesBeforeTestString);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return Integer.parseInt(issuesBeforeTestString);
    }

    private String getActualIssueType() {
        return issueDetailsDialog.getIssueType();
    }

    private String getActualIssueState() {
        return issueDetailsDialog.getIssueState();
    }

    private String getActualIssuePriority() {
        return issueDetailsDialog.getIssuePriority();
    }
}
