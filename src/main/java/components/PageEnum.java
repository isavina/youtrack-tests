package components;

public enum PageEnum {
    ISSUES("Issues", "JetBrains YouTrack"),
    DASHBOARDS("Dashboards", "YouTrack"),
    AGILE_BOARDS("Agile Boards", "YouTrack Agile Board"),
    REPORTS("Reports", "Reports"),
    PROJECTS("Projects", "Projects"),
    MORE("More", "");

    private final String titleValue;
    private final String pageTitle;

    PageEnum(String titleValue, String pageTitle) {
        this.titleValue = titleValue;
        this.pageTitle = pageTitle;
    }

    public String getTitleValue() {
        return titleValue;
    }

    public String getPageTitle() {
        return pageTitle;
    }
}
