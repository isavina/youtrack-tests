package components.models;

import components.IssuePriorityEnum;
import components.IssueStateEnum;
import components.IssueTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Issue {

    @NonNull
    private String summary;
    @NonNull
    private String description;
    private IssueTypeEnum type;
    private IssueStateEnum state;
    private IssuePriorityEnum priority;
//    private String assignee;
//    private String subsystem;
//    private String fixVersions;
//    private String affectedVersions;
//    private String fixInBuild;
//    private String Estimation;
//    private String spentTime;
//    private String project;

}
