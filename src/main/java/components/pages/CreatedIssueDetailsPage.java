package components.pages;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static components.utils.Locators.byDataTest;

@Slf4j
public class CreatedIssueDetailsPage extends BasePageWithSearches {
    public static final By CREATED_ISSUE_SUMMARY_LOCATOR = byDataTest("ticket-summary");
    public static final By CREATED_ISSUE_DESCRIPTION_LOCATOR = By.xpath("//div[starts-with(@class,'description')]/p");
    public static final By COMMENT_LOCATOR = By.cssSelector("[data-test='wysiwyg-editor']");
    public static final By PROJECT_LOCATOR = By.cssSelector("div[title^='Project']");
    public static final By PRIORITY_LOCATOR = By.xpath("//*[starts-with(@title,'Priority')]//button/span");
    public static final By ISSUE_TYPE_LOCATOR = By.xpath("//*[starts-with(@title,'Type')]//button/span");
    public static final By ISSUE_STATE_LOCATOR = By.xpath("//*[starts-with(@title,'State')]//button/span");
    public static final By BACK_TO_ISSUES_BTN_LOCATOR = By.cssSelector("a[href='issues'][class*='ring-ui-button']");
    public static final By ISSUE_ID_LOCATOR = By.cssSelector("a#id-link-id0");
    public static final By ISSUE_CREATED_ALERT_LOCATOR = By.xpath("//*[@data-test='alert']");
    public static final By ISSUE_ID_ALERT_LOCATOR = By.xpath("//*[@data-test='alert']//a/span");

    @Step("Go back to all issues of the project")
    public IssuesPage pressBackToIssuesBtn() {
        $(BACK_TO_ISSUES_BTN_LOCATOR).click();
        return page(IssuesPage.class);
    }

    @Step("Get issue type")
    public String getIssueType() {
        return $(ISSUE_TYPE_LOCATOR).getText();
    }

    @Step("Get issue state")
    public String getIssueState() {
        return $(ISSUE_STATE_LOCATOR).getText();
    }

    @Step("Get issue id")
    public String getIssueId() {
        return $(ISSUE_ID_LOCATOR).getText();
    }

    @Step("Get issue priority")
    public String getIssuePriority() {
        return $(PRIORITY_LOCATOR).getText();
    }

}
