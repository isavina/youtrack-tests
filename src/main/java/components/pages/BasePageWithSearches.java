package components.pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

@Slf4j
public class BasePageWithSearches extends BasePage {

    public static final By SEARCH_ASSIGNED_ISSUES_LOCATOR = By.cssSelector("span[title='Assigned to me']");
    public static final By SEARCH_COMMENTED_ISSUES_LOCATOR = By.cssSelector("span[title='Commented by me']");
    public static final By SEARCH_BOARD_BACKLOG_LOCATOR = By.cssSelector("span[title='Demo project Board Backlog']");
    public static final By SEARCH_REPORTED_ISSUES_LOCATOR = By.cssSelector("span[title='Reported by me']");
    public static final By SEARCH_UNASSIGNED_ISSUES_LOCATOR = By.cssSelector("span[title='Unassigned in DEMO']");
}
