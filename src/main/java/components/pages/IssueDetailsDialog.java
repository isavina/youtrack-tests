package components.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static components.utils.Locators.byDataTest;

public class IssueDetailsDialog extends CreatedIssueDetailsPage {
    public static final By SHOW_ALL_FIELDS_LOCATOR = byDataTest("ring-link fields-expander");

    @Step("Expand all fields of the issue")
    public IssueDetailsDialog expandIssueFields() {
        $(SHOW_ALL_FIELDS_LOCATOR).click();
        return this;
    }

    @Step("Get issue type")
    public String getIssueType() {
        return $(ISSUE_TYPE_LOCATOR).getText();
    }

    @Step("Get issue state")
    public String getIssueState() {
        return $(ISSUE_STATE_LOCATOR).getText();
    }

}
