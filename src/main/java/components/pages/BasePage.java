package components.pages;

import com.codeborne.selenide.Condition;
import components.PageEnum;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static components.utils.Locators.*;

@Slf4j
public class BasePage {

    public static final By ISSUES_TAB_LOCATOR = byNgHref(PageEnum.ISSUES.getTitleValue().toLowerCase());
    public static final By DASHBOARDS_TAB_LOCATOR = byDataTestId(PageEnum.DASHBOARDS.getTitleValue());
    public static final By AGILE_BOARDS_TAB_LOCATOR = byDataTestId(PageEnum.AGILE_BOARDS.getTitleValue());
    public static final By REPORTS_TAB_LOCATOR = byDataTestId(PageEnum.REPORTS.getTitleValue());
    public static final By PROJECTS_TAB_LOCATOR = byDataTestId(PageEnum.PROJECTS.getTitleValue());
    public static final By MORE_TAB_LOCATOR = byDataTestId("");
    public static final By AVATAR_TAB_LOCATOR = byDataTest("avatar");

    @Step("Open Issues tab")
    public IssuesPage openIssuesTab() {
        $(ISSUES_TAB_LOCATOR).click();
        $(By.cssSelector("title")).should(Condition.ownText("issues"));
        return page(IssuesPage.class);
    }
}
