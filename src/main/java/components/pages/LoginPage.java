package components.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static components.utils.Locators.byDataTest;

@Slf4j
public class LoginPage {

    public static By USERNAME_FIELD = By.id("username");
    public static By PASSWORD_FIELD = By.id("password");
    public static By LOG_IN_BTN = byDataTest("login-button");

    @Step("Validate log in page")
    public void validate() {
        $(USERNAME_FIELD).shouldBe(visible);
//        $("title").shouldHave(attribute("text", "Log in to YouTrack"));
    }

    @Step("Fill username")
    public LoginPage setUsername(String username) {
        $(USERNAME_FIELD).val(username);
        return page(LoginPage.class);
    }

    @Step("Fill password")
    public LoginPage setPassword(String password) {
        $(PASSWORD_FIELD).shouldBe(visible)
                .shouldBe(Condition.enabled)
                .shouldBe(Condition.empty)
                .val(password);
        return page(LoginPage.class);
    }

    @Step("Press Log in")
    public BasePage clickLoginBtn() {
        $(LOG_IN_BTN).shouldBe(visible)
                .shouldBe(Condition.enabled)
                .click();
        return page(BasePage.class);
    }

}
