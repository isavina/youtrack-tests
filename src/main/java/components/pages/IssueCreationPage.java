package components.pages;

import components.IssuePriorityEnum;
import components.IssueStateEnum;
import components.IssueTypeEnum;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static components.utils.Locators.byDataTest;

@Slf4j
public class IssueCreationPage extends BasePageWithSearches {
    public static final By CREATE_ISSUE_BTN_LOCATOR = byDataTest("submit-button");
    public static final By CREATION_OPTIONS_LOCATOR = byDataTest("creation_options");
    public static final By ISSUE_SUMMARY_LOCATOR = byDataTest("summary");
    public static final By ISSUE_DESCRIPTION_LOCATOR = byDataTest("wysiwyg-editor");
    public static final By ISSUE_DESCRIPTION_PLACE_LOCATOR = By.xpath("//*[@data-test='wysiwyg-editor']/*");
    public static final By PROJECT_SELECTION_LOCATOR = By.cssSelector("div[title^='Project']");
    public static final By PRIORITY_SELECTION_LOCATOR = By.cssSelector("div[title^='Priority']");
    public static final By TYPE_SELECTION_LOCATOR = By.cssSelector("div[title^='Type']");
    public static final By STATE_SELECTION_LOCATOR = By.cssSelector("div[title^='State']");

    @Step("Fill issue summary")
    public IssueCreationPage fillSummary(String issueSummary) {
        $(ISSUE_SUMMARY_LOCATOR).val(issueSummary);
        return this;
    }

    @Step("Fill issue description")
    public IssueCreationPage fillDescription(String issueDescription) {
        $(ISSUE_DESCRIPTION_LOCATOR).click();
        $(ISSUE_DESCRIPTION_PLACE_LOCATOR).val(issueDescription);
        return this;
    }

    @Step("Submit issue")
    public CreatedIssueDetailsPage submitIssue() {
        $(CREATE_ISSUE_BTN_LOCATOR).click();
        return page(CreatedIssueDetailsPage.class);
    }

    @Step("Select issue type")
    public IssueCreationPage selectIssueType(IssueTypeEnum issueType) {
        $(TYPE_SELECTION_LOCATOR).click();
        $(By.cssSelector(String.format("span[title=%s]", issueType.getTitleValue()))).click();
        return this;
    }

    @Step("Select issue state")
    public IssueCreationPage selectIssueState(IssueStateEnum issueState) {
        $(STATE_SELECTION_LOCATOR).click();
        $(By.cssSelector(String.format("span[title=\'%s\']", issueState.getTitleValue()))).click();
        return this;
    }

    @Step("Select issue priority")
    public IssueCreationPage selectIssuePriority(IssuePriorityEnum issuePriority) {
        $(PRIORITY_SELECTION_LOCATOR).click();
        $(By.cssSelector(String.format("span[title=\'%s\']", issuePriority.getTitleValue()))).click();
        return this;
    }
}


