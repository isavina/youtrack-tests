package components.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.*;
import static components.utils.Locators.byDataTest;
import static components.utils.Locators.byDataTestTitle;

@Slf4j
public class IssuesPage extends BasePageWithSearches {
    public static final By NEW_ISSUE_BUTTON_LOCATOR = byDataTest("createIssueButton");
    public static final By TITLE_LOCATOR = By.cssSelector("span.searchName__c1c");
    public static final By ISSUES_AMOUNT_LOCATOR = By.xpath("//*[contains(@data-test, 'issue-list-counter')]/span[@class]");
    public static final By PROJECT_SELECTION_LOCATOR = By.cssSelector("[aria-label='Project']");
    public static final By STATE_SELECTION_LOCATOR = By.cssSelector("[aria-label='State']");
    public static final By ASSIGNEE_SELECTION_LOCATOR = By.cssSelector("[aria-label='Assignee']");
    public static final By TYPE_SELECTION_LOCATOR = By.cssSelector("[aria-label='Type']");
    public static final By SWITCH_TO_QUERY_LOCATOR = byDataTest("ring-link filters-query-toggle");
    public static final By FIND_ISSUE_INPUT_LOCATOR = By.cssSelector("[aria-label^='Find issues']");
    public static final By ADD_FILTER_LOCATOR = By.cssSelector("[data-test-title='Filter Settings']");
    public static final By ISSUES_TABLE_LOCATOR = byDataTest("ring-table-wrapper");
    public static final By ISSUES_TABLE_ELEMENTS_LOCATOR = By.cssSelector("span.summary__bb7");
    public static final By ISSUE_CREATED_ALERT_LOCATOR = By.xpath("//*[@data-test='alert']");
    public static final By ISSUE_ID_ALERT_LOCATOR = By.xpath("//*[@data-test='alert']//a/span");
    Integer tableRowsAmount;
    String issuesAmount;

    @Step("Open issue creation dialog")
    public IssueCreationDialog openCreateIssueDialog() {
        $(NEW_ISSUE_BUTTON_LOCATOR).click();
        return new IssueCreationDialog();
    }

    @Step("Open issue creation dialog using hot keys")
    public IssueCreationDialog openCreateIssueDialogHotKeys() {
        $(NEW_ISSUE_BUTTON_LOCATOR).click();
        Selenide.actions().sendKeys(Keys.ALT, Keys.INSERT);
        return new IssueCreationDialog();
    }

    @Step("Select issues project")
    public IssuesPage pressSelectProjectBtn() {
        $(PROJECT_SELECTION_LOCATOR).click();
        return this;
    }

    @Step("Select issues state")
    public IssuesPage pressSelectStateBtn() {
        $(STATE_SELECTION_LOCATOR).click();
        return this;
    }

    @Step("Select assignee of issues")
    public IssuesPage pressSelectAssigneeBtn() {
        $(ASSIGNEE_SELECTION_LOCATOR).click();
        return this;
    }

    @Step("Select issues type")
    public IssuesPage pressSelectTypeBtn() {
        $(TYPE_SELECTION_LOCATOR).click();
        return this;
    }

    @Step("Switch to query search mode")
    public IssuesPage pressSwitchToQueryBtn() {
        $(SWITCH_TO_QUERY_LOCATOR).click();
        return this;
    }

    @Step("Select issues search field")
    public IssuesPage selectKeyWordInput() {
        $(FIND_ISSUE_INPUT_LOCATOR).click();
        return this;
    }

    @Step("Press Add filter")
    public IssuesPage pressAddFilterBtn() {
        $(ADD_FILTER_LOCATOR).click();
        return this;
    }

    @Step("Check table with issues is presented")
    public IssuesPage issuesTableIsPresented() {
        $(ISSUES_TABLE_LOCATOR).shouldBe(Condition.enabled, Condition.visible);
        return this;

    }

    @Step("Get rows amount in issues table")
    public Integer getTableRowsAmount() {
        tableRowsAmount = $$(ISSUES_TABLE_ELEMENTS_LOCATOR).shouldHave(CollectionCondition.sizeGreaterThan(0)).size();
        return tableRowsAmount;
    }

    @Step("Find issues counter value")
    public String getIssuesCounterValue() {
        issuesAmount = $(ISSUES_AMOUNT_LOCATOR).getText();
        return issuesAmount;
    }

    @Step("Check issue created alert appears")
    public String checkAlertAppearsAndDisappears() {
        $(ISSUE_CREATED_ALERT_LOCATOR).shouldBe(Condition.visible);
        String issueId = $(ISSUE_ID_ALERT_LOCATOR).getText();
        $(ISSUE_CREATED_ALERT_LOCATOR).should(Condition.disappear);
        return issueId;
    }

    @Step("Check alert doesn't appear")
    public void checkAlertNotAppeared() {
        $(ISSUE_CREATED_ALERT_LOCATOR).shouldNot(Condition.appear).shouldNotBe(Condition.visible);
    }

    @Step("Create button is not presented")
    public void createBtnNotFound() {
        $(NEW_ISSUE_BUTTON_LOCATOR).shouldNotBe(Condition.visible);
    }

    @Step("Open issue details in quick view")
    public IssueDetailsDialog openIssueDetails(String issueTitle) {
        $(byDataTestTitle(issueTitle)).click();
        return page(IssueDetailsDialog.class);
    }

    @Step("Open issue details in quick view")
    public IssuesPage refreshPage() {
        Selenide.refresh();
        return this;
    }
}
