package components;

public enum IssueTypeEnum {
    BUG("Bug"),
    COSMETICS("Cosmetics"),
    EXCEPTION_TYPE("Exception"),
    FEATURE("Feature"),
    TASK("Task"),
    USABILITY_PROBLEM("Usability Problem"),
    PERFORMANCE_PROBLEM("Performance Problem"),
    EPIC("Epic");


    private final String titleValue;

    IssueTypeEnum(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getTitleValue() {
        return titleValue;
    }
}
