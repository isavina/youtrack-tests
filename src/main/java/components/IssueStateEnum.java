package components;

public enum IssueStateEnum {
    SUBMITTED("Submitted"),
    OPEN("Open"),
    IN_PROGRESS("In Progress"),
    TO_BE_DISCUSSED("To be discussed"),
    REOPENED("Reopened"),
    CANT_REPRODUCE("Can't Reproduce"),
    DUPLICATE("Duplicate"),
    FIXED("Fixed"),
    WONT_FIX("Won't fix"),
    INCOMPLETE("Incomplete"),
    OBSOLETE("Obsolete"),
    VERIFIED("Verified");


    private final String titleValue;

    IssueStateEnum(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getTitleValue() {
        return titleValue;
    }
}
