package components.utils;

import com.codeborne.selenide.Selectors;
import org.openqa.selenium.By;

public class Locators {

    public static By byDataTestId(String dataTestId) {
        return Selectors.byCssSelector(String.format("[data-test-id='%s']", dataTestId));
    }

    public static By byDataTest(String dataTest) {
        return Selectors.byCssSelector(String.format("[data-test='%s']", dataTest));
    }

    public static By byNgHref(String ngHref) {
        return Selectors.byCssSelector(String.format("[ng-href='%s']", ngHref));
    }

    public static By byDataTestTitle(String dataTest) {
        return Selectors.byCssSelector(String.format("[data-test-title='%s']", dataTest));
    }
}
