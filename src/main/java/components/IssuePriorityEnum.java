package components;

public enum IssuePriorityEnum {

    STOPPER("Show-stopper"),
    CRITICAL("Critical"),
    MAJOR("Major"),
    NORMAL("Normal"),
    MINOR("Minor");

    private final String titleValue;

    IssuePriorityEnum(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getTitleValue() {
        return titleValue;
    }
}
