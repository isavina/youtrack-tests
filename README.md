# youtrack-tests

Repository for UI YouTrack tests.

Technology stack: Java11, Maven, TestNG, Selenide.

## Requirements
- Installed [Java 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html#license-lightbox).
- [Maven 3](https://maven.apache.org/download.cgi).

## Get started
1. Specify system environment variable JAVA_HOME to jdk bin.
2. Specify Maven .m2 local repository in system environment variable M2_HOME.
3. Clone project from Gitlab and open it in IDE.
4. Wait for indexing completion.

## Test configuration
Tests will be run in headless mode in Chrome browser on default environment. To change application configuration,
edit values in **application.properties**. To change headless mode, edit Selenide Configuration in **BaseTest**.

## Local test execution
To run default test suite, execute in IDE terminal console:
> mvn clean compile test

Another option is to [create TestNG configuration](https://www.jetbrains.com/help/idea/run-debug-configuration-testng.html#config)
and press Run.
### Local report generation
After tests running finished, Allure report can be generated and opened:
>mvn allure:serve

##Remote test execution and report
Currently, test execution is triggered by commit. Tests are run on shared Gitlab runner without proper configuration.
Test job fails due to not instantiated remote driver. Anyway, report is being generated and can be found:

1. Open pipeline, open report stage job.
2. Press **Browse** > target > site > allure-maven-plugin > index.html

## To be done
In the current implementation there are several gaps to be then done later.

:warning: Business gaps:
* Issues creation check is only implemented for Issues part of GUI. It is not only possible option. Besides, 
it could be done via Dashboards, Agile Boards and Backlog. These tests also could be covered, but several new 
pages and models should be implemented.
* Created issues could be checked in different places including Agile Boards, Assigned to, backlog etc.

:warning: Technical gaps:
* Selenide checks could be extended by additional shouldBe, shouldHave etc.
* Log in and log out tests could be separated from issues creation methods, but for the current purpose it is
not critical at all.
* Issue model could be extended by additional fields.
* Several profiles for different testng suits execution could be supported by maven pom.xml.
* Remote test execution could be improved. VM should be configured as remote host for Selenide tests execution
to make proper remote test execution possible.
* User credentials could be stored externally for more secure test execution.
* Allure server should be configured as a separate instance (VM, microservice). It allows to use this server
as a remote one in CI.